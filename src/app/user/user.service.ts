import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  login(userData): Observable<any> {
    return this.http.get('https://MY-API-CHANGE-THIS.net/login', {
      params: userData
    });
  }
}
