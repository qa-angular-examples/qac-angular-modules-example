import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/user/user.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public form: FormGroup;
  public currentError: string;

  constructor(
    private userService: UserService,
    private router: Router
  ) {
    this.form = new FormGroup({
      username: new FormControl('change this', Validators.required),
      password: new FormControl('', [
        Validators.required, Validators.minLength(8)
      ])
    });

    // this.form = new FormBuilder().group({
    //   username: ['change this'],
    //   password: ['']
    // });
  }

  ngOnInit() {
    this.form.valueChanges.subscribe(() => {
      console.log('CHANGE:', this.form.valid);
    });
  }

  handleSubmit() {
    if (this.form.valid) {
      this.currentError = null;
      this.userService.login(this.form.value).subscribe(() => {
        this.router.navigate(['home']);
      }, err => {
        this.currentError = err.error.message;
      });

    }
  }

}
